//
//  LMCompletionOption.m
//  FriedText
//
//  Created by Micha Mazaheri on 2013-09-06.
//  Copyright (c) 2013 Lucky Marmot. All rights reserved.
//

#import "LMCompletionOption.h"

@implementation LMCompletionOption

- (NSNumber *)weight
{
	return _weight ?: @(1);
}

- (NSString *)keywords
{
	return _keywords ?: _stringValue;
}

@end
